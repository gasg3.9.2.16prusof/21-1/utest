package pe.edu.uls.utest.dobles;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UtilText {

	private static final String formato = "yyyy/MM/dd";

	private SenderEmail senderEmail;
	
	private SenderSms senderSms;
	
	private ReaderProperties readerProps;
	
	public boolean enviarEmail(String email, String texto) {
		// otro codigo
		return senderEmail.sendEmail(email, texto);
	}
	
	public boolean enviarSms(String numero, String texto) {
		return senderSms.sendSms(numero, texto);
	}
	
	public String leerPropiedad(String archivo, String propiedad) {
		return readerProps.readProp(archivo, propiedad);
	}
	
	public String encriptar(String texto) {
		System.out.println("metodo encriptar aun no implementado");
		return null;
	}
	
	public Date aDate(String dateStr, int hora, int minuto) {
		if (hora >= 0 && hora <=23 && minuto >= 0 && minuto <= 59) {
			return aCalendar(dateStr, formato, hora, minuto).getTime();
		}
		return null; 
	}
	
	public Calendar aCalendar(String dateStr, String formato, int horaDelDia, int minuto) {
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		Date d = null;
		try {
			d = sdf.parse(dateStr);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	    Calendar cal = Calendar.getInstance();
	    cal.setTimeInMillis(d.getTime());
	    cal.set(Calendar.HOUR_OF_DAY, horaDelDia);
	    cal.set(Calendar.MINUTE, minuto);
	    return cal;
	}
	
}
